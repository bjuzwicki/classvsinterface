﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassVsInterface.BaseClass;
using ClassVsInterface.Interfaces;

namespace ClassVsInterface.Classes
{
    class Car : IMachine
    {
        public string Name {get; set;} = "Car";

        public void Run()
        {
            Console.WriteLine("chair preparation");
            Console.WriteLine("gears preparation");
            Console.WriteLine("run engine");
        }
    }
}
