﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassVsInterface.BaseClass;
using ClassVsInterface.Interfaces;

namespace ClassVsInterface.Classes
{
    class Tractor : IMachine
    {
        public string Name {get;set;} = "Tractor";

        public void Run()
        {
            Console.WriteLine("chair preparation");
            Console.WriteLine("puts on glasses");
            Console.WriteLine("gears preparation");
            Console.WriteLine("run engine");
    
        }
    }
}
