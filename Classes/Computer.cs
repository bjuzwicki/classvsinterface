﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassVsInterface.BaseClass;
using ClassVsInterface.Interfaces;

namespace ClassVsInterface.Classes
{
    class Computer : IMachine
    {
        public string Name {get; set;} = "Computer";

        public void Run()
        {
            Console.WriteLine("chair preparation");
            Console.WriteLine("headphones put on the ears");
            Console.WriteLine("paper preparation");
            Console.WriteLine("click power button");
        }
    }
}
