﻿using System;
using System.Collections.Generic;
using System.Text;
using ClassVsInterface.BaseClass;
using ClassVsInterface.Interfaces;

namespace ClassVsInterface.Classes
{
    class Jet : IMachine
    {
        public string Name {get;set;} = "Jet";

        public void Run()
        {
            Console.WriteLine("chair preparation");
            Console.WriteLine("headphones put on the ears");
            Console.WriteLine("aim preparation");
            Console.WriteLine("run engine");
        }
    }
}
