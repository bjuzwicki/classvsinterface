﻿﻿using ClassVsInterface.Interfaces;
using ClassVsInterface.Classes;
using System;
using System.Collections.Generic;


namespace ClassVsInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Tractor tractor = new Tractor();

            Computer computer = new Computer();

            Jet jet = new Jet();

            Car car = new Car();

            Paladog paladog = new Paladog();

            List<IMachine> machinesList = new List<IMachine>();

            machinesList.Add(tractor);
            machinesList.Add(computer);
            machinesList.Add(jet);
            machinesList.Add(car);
            machinesList.Add(paladog);

            foreach(var item in machinesList)
            {
                Console.WriteLine(item.Name + ":");
                item.Run();   
                Console.WriteLine("");
            }
        }
    }
}
