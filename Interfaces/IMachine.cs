﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassVsInterface.Interfaces
{
    interface IMachine
    {
        public string Name {get;set;}
        
        public void Run();   
    }
}
